package com.xiaoshu.aspect;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.alibaba.fastjson.JSONObject;
import com.xiaoshu.annotation.Log;
import com.xiaoshu.constant.Constants;
import com.xiaoshu.manager.TokenManager;
import com.xiaoshu.model.TokenModel;
import com.xiaoshu.service.SysLogService;
import com.xiaoshu.util.HttpHelper;
import com.xiaoshu.util.IpHelper;

/**
 * 
 * 功能说明：
 * 
 * LogInterceptor.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月29日上午10:08:41
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
@Aspect
@Component
public class LogAspect {

	@Autowired
	TokenManager tokenManager;
	
	@Autowired(required=false)
	SysLogService service;
	
	private static final String LOG_CONTENT = "[类名]:%s,[方法参数]:%s";
	private static final String annotation_scan = "@annotation(com.xiaoshu.annotation.Log)";
	
	@Pointcut(annotation_scan)
	private void anyMethod(){
		
	}
	
	@Around("anyMethod()")
	public Object invoke(ProceedingJoinPoint joinPoint) throws Throwable{
		HttpServletRequest request = HttpHelper.getHttpServletRequest();
		 //获取ip地址
		String ipAddress = 	IpHelper.getIpAddr(request);
		//获取用户id
		Integer userId=getUserId(request);
		
		MethodSignature signature = (MethodSignature)joinPoint.getSignature();
		Method method = signature.getMethod();
		Log log = method.getAnnotation(Log.class);
		//获取操作类型
		String operation = log.value();
		String operationType = log.type().toString();
		//获取类名
		String className = joinPoint.getTarget().getClass().getName();
		//获取参数
		Object[] params = joinPoint.getArgs();
		String paramsStr = getParamStr(params,method);
		
		String content =  String.format(LOG_CONTENT, className, method.getName()+paramsStr );
		StopWatch clock = new StopWatch();
		clock.start();
		Object object = joinPoint.proceed();
		clock.stop();
		// 保存日志信息到数据库
		service.saveLog(userId, content, ipAddress, operation,operationType,clock.getTotalTimeSeconds());
		return object;
	}
	
	private Integer getUserId(HttpServletRequest request) {
		// 从head中获取验证信息
		String authentication = request.getHeader(Constants.AUTHORIZATION);
		if (authentication != null) {
			TokenModel model = tokenManager.getToken(authentication);
			if (model != null) {
				return model.getUserId();
			}
		}
		return 0;
	}
	
	private String getParamStr(Object[] params, Method method) {
		StringBuffer bf = new StringBuffer("(");
		Class<?>[] clazzs = method.getParameterTypes();
		for(int i=0;i<params.length;i++){
			Object obj = params[i];
			Class<?> clazz = clazzs[i];
			
			//如果是基本类型，
			if(clazz.getClass().isPrimitive()){
				String typeName = clazz.getSimpleName();
				bf.append(typeName).append(" ");
				bf.append(obj).append(",");
			}else{
				String typeName = clazz.getName();
				bf.append(typeName).append(" ");
				bf.append(JSONObject.toJSONString(obj)).append(",");
			}
		}
		bf.deleteCharAt(bf.length()-1);
		bf.append(")");
		return bf.toString();
	}
}
