package com.xiaoshu.aspect;

import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xiaoshu.annotation.Cache;
import com.xiaoshu.manager.JedisManager;

/**
 * 
 * 功能说明：通过AOP的方式，进行缓存方法结果集，如果缓存中命中，则不会执行该方法 。   *为防止数据不准确，请尽量设置过期时间为一个合理的值
 * 
 * CacheInterceptor.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月7日上午10:47:36
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
@Component
@Aspect
public class CacheAspect {

	private Logger logger = Logger.getLogger(CacheAspect.class);
	
	@Autowired
	private JedisManager jedis;
	
	private static final String KEY_PREFIX="cache:bean:";
	private static final String annotation_scan = "@annotation(com.xiaoshu.annotation.Cache)";
	
	@Pointcut(annotation_scan)
	private void anyMethod(){
		
	}
	
	@Around("anyMethod()")
	public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable {
		Object object = invoke(pjp);// 执行该方法
		return object;
	}
	
	private Object invoke(ProceedingJoinPoint pjp) throws Throwable {
		
		try {
			MethodSignature signature = (MethodSignature) pjp.getSignature();
			Method method = signature.getMethod();
			// 判断该方法上是否使用有 Cache注解，没有则直接返回
			if (!method.isAnnotationPresent(Cache.class)) {
				return pjp.proceed();
			}
			
			Cache annotation = method.getAnnotation(Cache.class);
			String key = annotation.key();
			int expire = annotation.expire();
			Class<?> clazz= method.getReturnType();
			Object[] params = pjp.getArgs();
			String paramStr = getParamStr(params);
			String name = method.getDeclaringClass().getName()+"."+method.getName()+paramStr;
			if(StringUtils.isEmpty(key)){
				key = KEY_PREFIX+name;
			}else{
				key = KEY_PREFIX+name+":"+key;
			}
			Object obj = jedis.getByKryo(key, clazz,expire);
			if(obj==null){
				 obj =  pjp.proceed();
				 if(obj!=null){
					 jedis.setByKryo(key, obj, expire);
				 }
			}
			return obj;
		} catch (Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
		}

		return pjp.proceed();
	}
	
	private String getParamStr(Object[] params) {
		StringBuffer bf = new StringBuffer("(");
		for(Object obj:params){
			bf.append(obj).append(",");
		}
		bf.deleteCharAt(bf.length()-1);
		bf.append(")");
		return bf.toString();
	}
}
