package com.xiaoshu.aspect;

import java.lang.reflect.Method;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import com.xiaoshu.annotation.AppAuthorization;
import com.xiaoshu.constant.Constants;
import com.xiaoshu.manager.TokenManager;

/**
 * 
 * 功能说明：拦截 AppAuthorization 注解信息
 * 
 * AppAspect.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年10月10日下午5:20:46
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
@Aspect
public class AppAspect {
	
	@Autowired
	private TokenManager tokenManager;

	@Pointcut("@annotation(com.xiaoshu.annotation.AppAuthorization)")
	public void anyMethod(){
		
	}
	
	@Around("anyMethod()")
	public Object doExecute(ProceedingJoinPoint joinPoint) throws Throwable{
		// 从切点上获取目标方法
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        
		// 若目标方法忽略了安全性检查，则直接调用目标方法
        if(!method.isAnnotationPresent(AppAuthorization.class) 
        		&&  !method.getClass().isAnnotationPresent(AppAuthorization.class)){
        	return joinPoint.proceed();
        }
        
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        HttpServletResponse response = sra.getResponse();
        String token = getAuthentication(request);
        //如果无效则抛出异常
		if(StringUtils.isEmpty(token) || !tokenManager.checkToken(token, Constants.APP_TOKEN_KEY_PREFIX)){
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "Token is null or Token is invalid");
			return null;
		}
		//如果app_token 有效则继续执行方法
		return joinPoint.proceed();
	}
	
	//从头部或者cookies中获得Token信息
	private String getAuthentication(HttpServletRequest request) {
		String authentication = request.getHeader(Constants.AUTHORIZATION);
		if (authentication == null) {
			Cookie[] cookies = request.getCookies();
			if (cookies == null) {
                return null;
            }
			for (Cookie cookie : cookies) {
				if (Constants.AUTHORIZATION.equals(cookie.getName())) {
					authentication = cookie.getValue();
					break;
				}
			}
		}
		return authentication;
	}
}
