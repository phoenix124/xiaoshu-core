package com.xiaoshu.aspect;

import java.lang.reflect.Method;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.common.json.JSON;
import com.xiaoshu.annotation.CacheType;
import com.xiaoshu.annotation.DBCache;
import com.xiaoshu.manager.JedisManager;
import com.xiaoshu.util.SerializableUtils;

/**
 * 
 * 功能说明：
 * 
 * DBCacheInterceptor.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:28:50
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Aspect
@Component
public class DBCacheAspect {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private JedisManager jedis;
	
	private static final String TABLE_PREFIX="cache:table:";
	
	private static final String DATA_PREFIX="cache:data:";
	
	private static final String package_scan = "execution(* com.ubtechinc.mapper.*.*(..))";

	@Pointcut(package_scan)
	private void anyMethod() {
	}

	@Around("anyMethod()")
	public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable {
		Object object = invoke(pjp);// 执行该方法
		return object;
	}

	private Object invoke(ProceedingJoinPoint pjp) throws Throwable {
		try {

			MethodSignature signature = (MethodSignature) pjp.getSignature();
			Method method = signature.getMethod();
			// 判断该方法上是否使用有 DBCacheAnnotation注解，没有则直接返回
			if (!method.isAnnotationPresent(DBCache.class)) {
				return pjp.proceed();
			}
			DBCache annotation = method.getAnnotation(DBCache.class);
			CacheType type = annotation.value();
			String[] tables = annotation.tables();
			// 根据不同的方式进行不同的处理
			switch (type) {
				case QUERY:
					return queryHandler(pjp,method,tables);
				case NOTIFY:
					return notifyHandler(pjp,method,tables);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return pjp.proceed();
	}

	private Object notifyHandler(ProceedingJoinPoint pjp, Method method, String[] tables) throws Throwable {
		Object result = pjp.proceed();
		//删除缓存相关表的数据
		removeCache(method,tables);
		return result;
	}

	private void removeCache(Method method, String[] tables) {
		// 生成hash key, 形式为table1~table2~table3~....
	    for(String table:tables){
	    	//获取所有相关连的表
	    	Set<String> members = jedis.smembers(TABLE_PREFIX+table);
	    	for(String member:members){
	    		//获取所有的hashkey的值
	    		byte[] hashKeyBytes  = member.getBytes();
	    		Set<byte[]> fields =  jedis.hkeys(member.getBytes());
	    		for(byte[] field:fields){
	    			jedis.hdel(hashKeyBytes, field);
	    		}
	    		//删除set数据
	    		jedis.srem(TABLE_PREFIX+table, member);
	    	}
	    }
	    
		
	}

	private Object queryHandler(ProceedingJoinPoint pjp, Method method, String[] tables) throws Throwable {
		String methodName = method.getName();
		Object result = null;
		
        // 生成hash key, 形式为table1~table2~table3~.....
        String hashKey = DATA_PREFIX+StringUtils.join(tables, "~");
        byte[] hashKeyBytes = hashKey.getBytes();
		 // 生成SQL key, 形式为 函数名:参数值1,参数值2,参数值3,....
        Object[] params = pjp.getArgs();
        String sqlKey = "";
        sqlKey = methodName + ":" ;
        if (params.length > 0) {
        	for(Object param:params){
        		sqlKey+= JSON.json(param);
        	}
           // sqlKey = methodName + ":" + StringUtils.join(params, ",");
        } else {
            sqlKey = methodName;
        }
        
        byte[] sqlKeyBytes = sqlKey.getBytes();
        //如果jedis已经存在值，则直接获取
        if(jedis.hexists(hashKeyBytes, sqlKeyBytes)){
        	byte[] bys = jedis.hget(hashKeyBytes, sqlKeyBytes);
        	result =  SerializableUtils.read(bys);
        	 logger.info("<-------- 缓存中获取数据，hashkey：" + StringUtils.toEncodedString(hashKeyBytes,null) + ",sqlKey:"
                     + StringUtils.toEncodedString(sqlKeyBytes,null) + "-------->");
        }else{
        	//调用方法查询结果
        	result = pjp.proceed();
        	byte[] value = SerializableUtils.write(result);
        	//将结果集放入缓存中
        	jedis.hset(hashKeyBytes, sqlKeyBytes, value);
        	
        	 // 将Key为table，value为table1~table2~table3~.....这种形式的Set，存入Redis，以便在该table发生写操作时，将其关联的多表联合查询产生的缓存清除掉。
            setJoinRelationshipCacheForSpecificTable( tables, hashKey);

            logger.info("<-------- 将数据库结果存入缓存，hashkey：" + StringUtils.toEncodedString(hashKeyBytes,null) + ",sqlKey:"
                    + StringUtils.toEncodedString(sqlKeyBytes,null) + "-------->");
        	
        }

		return result;
	}

	private void setJoinRelationshipCacheForSpecificTable( String[] tables, String hashKey) {
		for(String table:tables){
			jedis.sadd(TABLE_PREFIX+table, hashKey);
		}
		
	}
}
