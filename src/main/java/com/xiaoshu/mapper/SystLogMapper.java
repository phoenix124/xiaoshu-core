package com.xiaoshu.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xiaoshu.model.SysLog;

/**
 * @title 
 *
 * @author Young
 *
 * @date 2017年5月19日下午2:54:38
 *
 * Copyright (C)2012-2017 深圳优必选科技 All rights reserved.
 */
public interface SystLogMapper extends BaseMapper<SysLog> {

}
