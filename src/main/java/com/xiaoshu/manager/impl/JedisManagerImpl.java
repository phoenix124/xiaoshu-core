package com.xiaoshu.manager.impl;



import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import redis.clients.jedis.ShardedJedis;

import com.xiaoshu.manager.BilinShardedJedisPool;
import com.xiaoshu.manager.JedisManager;
import com.xiaoshu.util.KryoUtil;
/**
 * 
 * 功能说明： 
 * 
 * JedisManagerImpl.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:42:52
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Component("jedisManagerImpl")
public class JedisManagerImpl implements JedisManager  {
	

	private final static Log logger = LogFactory.getLog(JedisManagerImpl.class);

	@Autowired
	@Qualifier("jedis.pool")
	private BilinShardedJedisPool jedisPool;
	
	private final static int MAX_CACHE_SEC = 30 * 86400;// 30 days
	
	private final static int MIN_CACHE_SEC = 0;// 最短时间
	
	private final static int DEFAULT_CACHE_SEC = 86400; // 默认时间一天
	
	
	

	private int getRealCacheTime(int seconds) {
		// 最长只能放30天
		seconds = seconds > MAX_CACHE_SEC ? MAX_CACHE_SEC : seconds;
		return seconds < MIN_CACHE_SEC ? MIN_CACHE_SEC : seconds;
	}

	@Override
	public int getMaxCacheSec() {
		return MAX_CACHE_SEC;
	}

	@Override
	public int getMinCacheSec() {
		return MIN_CACHE_SEC;
	}

	@Override
	public int getDefaultCacheSec() {
		return DEFAULT_CACHE_SEC;
	}

	@Override
	public boolean setByKryo(String key, Object obj) {
		
		return setByKryo(key,obj,DEFAULT_CACHE_SEC);
	}

	@Override
	public boolean setByKryo(String key, Object obj, int seconds) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.setex(key.getBytes(),getRealCacheTime(seconds), KryoUtil.serialize(obj));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return false;
	}

	@Override
	public <T> T getByKryo(String key, Class<T> myClass) {
		return getByKryo(key,myClass,0);
	}
	
	@Override
	public <T> T getByKryo(String key, Class<T> myClass, int seconds) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			byte[] data =  jedis.get(key.getBytes());
			T obj = KryoUtil.deserialize(data, myClass);
			if(seconds>0){
				jedis.expire(key.getBytes(), getRealCacheTime(seconds));
			}
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return null;
	}


	@Override
	public boolean setnx(String key, String value) {
		ShardedJedis commonJedis = null;
		try {
			commonJedis = jedisPool.getResource();
			Long result = commonJedis.setnx(key, value);
			return (result != null && result == 1);
		} catch (Exception e) {
			logger.error("setnx cache error:", e);
			return false;
		} finally {
			if (commonJedis != null) {
				commonJedis.close();
			}
		}
	}

	@Override
	public void del(String key) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.del(key.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (jedis != null)
				jedis.close();
		}
	}

	
	@Override
	public String getString(String key) {
		ShardedJedis jedis = null;
		String value = null;
		try {
			jedis = jedisPool.getResource();
			value = jedis.get(key);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return value;
	}


	/*public void setString(String key, String value) {
		setString(key,value,DEFAULT_CACHE_SEC);
		
	}*/
	@Override
	public void setString(String key, String value) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		
	}
	
	@Override
	public void setString(String key, String value, int seconds) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
			jedis.expire(key, getRealCacheTime(seconds));
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		
	}


	@Override
	public boolean setnx(String key, String value, int seconds) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			Long result = jedis.setnx(key, value);
			if (result != null && result == 1) {
				jedis.expire(key, getRealCacheTime(seconds));
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (jedis != null)
				jedis.close();
		}
		return false;
	}


	@Override
	public void expire(String key, int seconds) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.expire(key, getRealCacheTime(seconds));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (jedis != null)
				jedis.close();
		}
		
	}
	
	@Override
	public void expireAt(String key, long seconds) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.expireAt(key, seconds);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (jedis != null)
				jedis.close();
		}
		
	}
	
	@Override
	public long getExpire(String key) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.ttl(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (jedis != null)
				jedis.close();
		}
		return 0;
	}


	@Override
	public long lpush(String key, String field) {
		return lpush(key,field,DEFAULT_CACHE_SEC);
	}
	
	@Override
	public long lpush(String key, String field, int seconds) {
		long r = 0;
		ShardedJedis commonJedis = null;
		try {
			commonJedis = jedisPool.getResource();
			r = commonJedis.lpush(key, field);
			commonJedis.expire(key, getRealCacheTime(seconds));
		} catch (Exception e) {
			logger.error("lpush cache error:", e);
			e.printStackTrace();
		} finally {
			if (commonJedis != null) {
				commonJedis.close();
			}
		}
		return r;
	}

	@Override
	public List<String> lrange(String key, long start, long end) {
		ShardedJedis commonJedis = null;
		try {
			commonJedis = jedisPool.getResource();
			return commonJedis.lrange(key, start, end);
		} catch (Exception e) {
			logger.error("lrange cache error:", e);
			e.printStackTrace();
		} finally {
			if (commonJedis != null) {
				commonJedis.close();
			}
		}
		return null;
	}


	@Override
	public Long ttl(String key) {
		ShardedJedis commonJedis = null;
		try {
			commonJedis = jedisPool.getResource();
			return commonJedis.ttl(key);
		} catch (Exception e) {
			logger.error("lrange cache error:", e);
			e.printStackTrace();
		} finally {
			if (commonJedis != null) {
				commonJedis.close();
			}
		}
		return null;
	}

	@Override
	public String getString(byte[] key) {
		ShardedJedis jedis = null;
		String value = null;
		try {
			jedis = jedisPool.getResource();
			value = jedis.get(key)!=null?new String(jedis.get(key)):null;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return value;
	}

	@Override
	public void setString(byte[] key, byte[] value) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		
	}

	@Override
	public void setString(byte[] key, byte[] value, int seconds) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
			jedis.expire(key, getRealCacheTime(seconds));
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		
	}

	@Override
	public void hset(String key, String field, String value) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hset(key, field, value);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		
	}

	@Override
	public void hset(byte[] key, byte[] field, byte[] value) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hset(key, field, value);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
	}

	@Override
	public String hget(String key, String field) {
		ShardedJedis jedis = null;
		String result=null;
		try {
			jedis = jedisPool.getResource();
			result =  jedis.hget(key, field);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return result;
	}

	@Override
	public byte[] hget(byte[] key, byte[] field) {
		ShardedJedis jedis = null;
		 byte[] result=null;
		try {
			jedis = jedisPool.getResource();
			result =  jedis.hget(key, field);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return result;
	}

	@Override
	public Map<String, String> getAll(String key) {
		ShardedJedis jedis = null;
		Map<String, String> result=null;
		try {
			jedis = jedisPool.getResource();
			result =  jedis.hgetAll(key);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return result;
	}

	@Override
	public Map<byte[], byte[]> getAll(byte[] key) {
		ShardedJedis jedis = null;
		Map<byte[], byte[]> result=null;
		try {
			jedis = jedisPool.getResource();
			result =  jedis.hgetAll(key);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return result;
	}

	@Override
	public void hdel(String key, String field) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hdel(key, field);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		
	}

	@Override
	public void hdel(byte[] key, byte[] field) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hdel(key, field);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		
	}

	@Override
	public Set<String> hkeys(String key) {
		ShardedJedis jedis = null;
		Set<String> keys = null;
		try {
			jedis = jedisPool.getResource();
			keys = jedis.hkeys(key);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return keys;
	}

	@Override
	public Set<byte[]> hkeys(byte[] key) {
		ShardedJedis jedis = null;
		Set<byte[]> keys = null;
		try {
			jedis = jedisPool.getResource();
			keys = jedis.hkeys(key);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return keys;
	}

	@Override
	public long sadd(byte[] key, byte[]... members) {
		ShardedJedis jedis = null;
		long result = 0;
		try {
			jedis = jedisPool.getResource();
			result = jedis.sadd(key,members);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return result;
	}

	@Override
	public long sadd(String key, String... members) {
		ShardedJedis jedis = null;
		long result = 0;
		try {
			jedis = jedisPool.getResource();
			result = jedis.sadd(key,members);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return result;
	}

	@Override
	public Set<String> smembers(String key) {
		ShardedJedis jedis = null;
		Set<String> members = null;
		try {
			jedis = jedisPool.getResource();
			members = jedis.smembers(key);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return members;
	}

	@Override
	public Set<byte[]> smembers(byte[] key) {
		ShardedJedis jedis = null;
		Set<byte[]> members = null;
		try {
			jedis = jedisPool.getResource();
			members = jedis.smembers(key);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return members;
	}

	@Override
	public boolean hexists(String key, String field) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.hexists(key,field);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return false;
	}

	@Override
	public boolean hexists(byte[] key, byte[] field) {
		ShardedJedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.hexists(key,field);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return false;
	}

	@Override
	public long srem(byte[] key, byte[]... members) {
		ShardedJedis jedis = null;
		long result = 0;
		try {
			jedis = jedisPool.getResource();
			result =  jedis.srem(key,members);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return result;
	}

	@Override
	public long srem(String key, String... members) {
		ShardedJedis jedis = null;
		long result = 0;
		try {
			jedis = jedisPool.getResource();
			result =  jedis.srem(key,members);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(jedis!=null)jedis.close();
		}
		return result;
	}

}
