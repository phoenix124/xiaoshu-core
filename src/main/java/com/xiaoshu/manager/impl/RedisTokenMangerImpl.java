package com.xiaoshu.manager.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.xiaoshu.constant.Constants;
import com.xiaoshu.manager.JedisManager;
import com.xiaoshu.manager.TokenManager;
import com.xiaoshu.model.TokenModel;

/**
 * 
 * 功能说明：
 * 
 * RedisTokenMangerImpl.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:42:46
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Component("tokenManager")
public class RedisTokenMangerImpl implements TokenManager {
	@Autowired
	private JedisManager jedisManager;
	
	private final String TOKEN_KEY_PREFIX="user:token:";
	
	@Override
	public TokenModel createToken(Integer userId, Integer appType) {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replaceAll("-", "");
		String token = uuid;
		
		TokenModel model = new TokenModel();
		model.setToken(token);
		model.setUserId(userId);
		
		
		//根据不同的app类型将token存入redis中
		if(allowMultiple(appType)){
			String key = TOKEN_KEY_PREFIX+token;
			jedisManager.setString(key, userId+"",Constants.TOKEN_EXPIRES_HOUR);
		}else{
			String key = TOKEN_KEY_PREFIX+userId;
			jedisManager.setString(key, token,Constants.TOKEN_EXPIRES_HOUR);
		}
		
		return model;
	}

	@Override
	public TokenModel createToken(String prefixName) {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replaceAll("-", "");
		String token = uuid;
		
		TokenModel model = new TokenModel();
		model.setToken(token);
		
		String key = prefixName+token;
		jedisManager.setString(key, token,Constants.TOKEN_EXPIRES_HOUR);
		return model;
	}

	/*
	 * 生成token
	 * 先生成一个32位的随机uuid，然后将userid直接拼接在后面
	 * 
	 * */
	@Override
	public TokenModel createToken(Integer userId) {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replaceAll("-", "");
		String token = uuid;
		
		TokenModel model = new TokenModel();
		model.setToken(token);
		model.setUserId(userId);
		
		//将token存入redis中
		String key = TOKEN_KEY_PREFIX+userId;
		jedisManager.setString(key, token,Constants.TOKEN_EXPIRES_HOUR);
		
		return model;
	}

	@Override
	public boolean checkToken(TokenModel model) {
		
		if(model==null){
			return false;
		}
		
		String key = TOKEN_KEY_PREFIX+model.getUserId();
		String token = jedisManager.getString(key);
		if(StringUtils.isEmpty(token)|| !token.equals(model.getToken())){
			return false;
		}
		
		//重新设置token过期时间
		jedisManager.expire(key, Constants.TOKEN_EXPIRES_HOUR);

		return true;
	}
	
	@Override
	public boolean checkToken(String token, String prefixName) {
		if(StringUtils.isEmpty(token) || StringUtils.isEmpty(prefixName)){
			return false;
		}
		
		String key = prefixName + token;
		String srcToken = jedisManager.getString(key);
		if(StringUtils.isEmpty(srcToken) || !srcToken.equals(token)){
			return false;
		}
		return true;
	}

	@Override
	public TokenModel getToken(String authentication) {
		if(authentication==null||authentication.length()<33){
			return null;
		}
		String token = authentication.substring(0, 32);
		Integer userId = Integer.valueOf(authentication.substring(32));
		
		TokenModel model = new TokenModel(token,userId);
		return model;
	}

	@Override
	public void deleteToken(long userId) {
		
		String key = TOKEN_KEY_PREFIX+userId;
		
		jedisManager.del(key);
		
	}
	
	public boolean allowMultiple(Integer appType){
		if(appType==null){
			return false;
		}
		return appType==1;
	}

}
