package com.xiaoshu.manager.impl;



import io.jsonwebtoken.Claims;

import javax.naming.OperationNotSupportedException;

import com.xiaoshu.manager.TokenManager;
import com.xiaoshu.model.TokenModel;
import com.xiaoshu.util.JwtUtil;


/**
 * 
 * 功能说明：
 * 
 * JwtTokenManagerImpl.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月7日上午11:00:24
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
//@Component(value="jwtTokenManagerImpl")
public class JwtTokenManagerImpl implements TokenManager {
	

	@Override
	public TokenModel createToken(Integer userId, Integer appType) {
		return null;
	}

	@Override
	public TokenModel createToken(String prefixName) {
		return null;
	}

	@Override
	public TokenModel createToken(Integer userId) {
		String token = JwtUtil.createJwt(userId+"", "getToken",-1);
		TokenModel model = new TokenModel();
		model.setToken(token);
		model.setUserId(userId);
		return model;
	}

	@Override
	public boolean checkToken(TokenModel model) {
		if(model==null) return false;
		return JwtUtil.parseJwt(model.getToken())!=null;
	}
	
	@Override
	public boolean checkToken(String token, String prefixName) {
		
		return false;
	}

	@Override
	public TokenModel getToken(String authentication) {
		Claims claims = JwtUtil.parseJwt(authentication);
		if(claims==null){
			return null;
		}
		String id = claims.getId();
		Integer userId = Integer.parseInt(id);
		TokenModel model = new TokenModel(authentication,userId);
		return model;
	}

	@Override
	public void deleteToken(long userId) {
		try {
			throw new OperationNotSupportedException();
		} catch (OperationNotSupportedException e) {
			e.printStackTrace();
		}

	}

}
