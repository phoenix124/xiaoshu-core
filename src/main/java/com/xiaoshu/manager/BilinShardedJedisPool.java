package com.xiaoshu.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.log4j.Logger;

import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

/**
 * 
 * 功能说明：
 * 
 * BilinShardedJedisPool.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:43:10
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
public class BilinShardedJedisPool {

	private Logger logger = Logger.getLogger(BilinShardedJedisPool.class);
	private ShardedJedisPool shardedJedisPool;
	
	  /**
     * add by fupf
     * @param poolConfig
     * @param configStr
     * @param timeout
     */
    public BilinShardedJedisPool(final GenericObjectPoolConfig poolConfig, String configStr, int timeout) {
        this(poolConfig, configStr, timeout, null);
    }

	public BilinShardedJedisPool(GenericObjectPoolConfig poolConfig, String configStr, int timeout,String password) {
		try{
			List<JedisShardInfo> infoList = new ArrayList<JedisShardInfo>();
			String[] configAgrs = configStr.split(",");
			for(String configAgr:configAgrs){
				String[] params = configAgr.split(":");
				String host = params[1];
				String name = params[0];
				int port = Integer.parseInt(params[2]);
				
				JedisShardInfo info = new JedisShardInfo(host,port,timeout,name);
				if (StringUtils.isNotEmpty(password)) {
                    info.setPassword(password);
                }
				infoList.add(info);
			}
			logger.debug("load redis pool size:" + infoList.size());
			shardedJedisPool = new ShardedJedisPool(poolConfig,infoList);
		}catch(Exception e){
			logger.error("redis配置格式非法:", e);
			throw new RuntimeException("redis配置格式非法:", e);
		}
	}
	
	
	public ShardedJedis getResource(){
		if(shardedJedisPool == null){
			return null;
		}
		
		ShardedJedis shardedJedis=null;
		try {
			shardedJedis = shardedJedisPool.getResource();
		} catch (Exception e) {
			logger.error("get jedisWrap from shardedJedisPool error", e);
			e.printStackTrace();
		}
		return shardedJedis;
	}
	
	
}
