package com.xiaoshu.manager;

import com.xiaoshu.model.TokenModel;


/**
 * 
 * 功能说明：
 * 
 * TokenManager.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:42:59
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
public interface TokenManager {

    /**
     * 创建一个token关联上指定用户
     * @param userId 指定用户的id
     * @param appType 
     * @return 生成的token
     */
    public TokenModel createToken(Integer userId, Integer appType);
    /**
     * 创建一个token关联上指定用户
     * @param userId 指定用户的id
     * @return 生成的token
     */
    public TokenModel createToken(Integer userId);
    
	public TokenModel createToken(String prefixName);

    /**
     * 检查token是否有效
     * @param model token
     * @return 是否有效
     */
    public boolean checkToken(TokenModel model);
    
    public boolean checkToken(String token, String prefixName);

    /**
     * 从字符串中解析token
     * @param authentication 加密后的字符串
     * @return
     */
    public TokenModel getToken(String authentication);

    /**
     * 清除token
     * @param userId 登录用户的id
     */
    public void deleteToken(long userId);

}
