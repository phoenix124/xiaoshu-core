package com.xiaoshu.manager;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * 功能说明：
 * 
 * JedisManager.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:43:05
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
public interface JedisManager {
	
	/***
	 * 获取最大缓存时间
	 * @return  最大缓存时间
	 */
	public int getMaxCacheSec();

	/***
	 * 获取最小缓存时间
	 * @return  最小缓存时间
	 */
	public int getMinCacheSec();

	/***
	 *获取默认缓存时间
	 * @return 默认缓存时间 （30天）
	 */
	public int getDefaultCacheSec();
	
	/***
	 * kryo进行组包解包,适用于缓存bean，
	 * @param key
	 * @param obj 对应的bean
	 * @return 
	 */
	public boolean setByKryo(String key,Object obj);
	
	/***
	 * kryo进行组包解包,适用于缓存bean,并设置过期时间
	 * @param key
	 * @param obj 对应的bean
	 * @param seconds 单位（秒）
	 * @return 
	 */
	public boolean setByKryo(String key,Object obj, int seconds);
	
	/***
	 * 获取值并适用kryo解包
	 * @param key
	 * @param myClass 目标对象的class
	 * 
	 * @return 目标对象
	 */
	public <T> T getByKryo(String key,Class<T> myClass);
	
	/***
	 * 获取值并适用kryo解包，并设置过期时间
	 * @param key
	 * @param myClass 目标对象的class
	 * @param seconds 单位（秒）
	 * @return 目标对象
	 */
	public <T> T getByKryo(String key,Class<T> myClass, int seconds);
	
	/***
	 * 删除数据
	 * @param key
	 */
	public void del(String key);
	

	/***
	 * 设置过期时间
	 * @param key
	 * @param seconds 单位（秒）
	 */
	public void expire(String key, int seconds);
	
	/***
	 * 设置过期时间点，及设置在某个时间点过期
	 * @param key
	 * @param unixTime 单位（毫秒）
	 */
	public void expireAt(String key, long unixTime);
	
	/***
	 * 获取过期时间
	 * @param key
	 * @return 过期时间
	 */
	public long getExpire(String key);
	
	/***
	 * 获取String结构的value
	 * @param key
	 * @return
	 */
	public String getString(String key);
	
	/***
	 * 获取String结构的value
	 * @param key
	 * @return
	 */
	public String getString(byte[] key);
	
	/***
	 * 设置String结构的value
	 * @param key
	 * @param value
	 * @return
	 */
	public void setString(String key, String value);
	
	/***
	 * 设置String结构的value
	 * @param key
	 * @param value
	 * @return
	 */
	public void setString(byte[] key, byte[] value);
	
	/***
	 * 设置String结构的value，并设置过期时间
	 * @param key
	 * @param value
	 * @param seconds 单位（秒）
	 * @return
	 */
	public void setString(String key, String value, int seconds);
	
	
	/***
	 * 设置String结构的value，并设置过期时间
	 * @param key
	 * @param value
	 * @param seconds 单位（秒）
	 * @return
	 */
	public void setString(byte[] key, byte[] value, int seconds);
	
	
	
	
	/***
	 * 如果值不存在，设置String结构的value，并设置过期时间
	 * @param key
	 * @param value
	 * @param seconds 单位（秒）
	 * @return
	 */
 	public boolean setnx(String key, String value,int seconds);
 	
 	/***
	 * 如果值不存在，设置String结构的value
	 * @param key
	 * @param value
	 * @return
	 */
 	public boolean setnx(String key, String value);
 	

 	/***
	 * list中插入值
	 * @param key
	 * @param value
	 * @return
	 */
 	public long lpush(String key, String value);
 	
 	/***
	 * list中插入值,并设置过期时间
	 * @param key
	 * @param value
	 * @param seconds 单位（秒）
	 * @return
	 */
 	public long lpush(String key, String value, int seconds);
 	
 	/***
	 * list 获取值
	 * @param key
	 * @param start 
	 * @param end 
	 * @return
	 */
 	public List<String> lrange(String key, long start, long end);
 	
 	/***
	 * 获取剩余过期时间
	 * @param key
	 * @return 剩余过期时间（秒）
	 */
 	public Long ttl(String key);
 	
 	
 	//hash表相关操作
 	public void hset(String key,String field,String value);
 	
 	public void hset(byte[] key,byte[] field,byte[] value);
 	
 	public String hget(String key,String field);
 	
 	public byte[] hget(byte[] key,byte[] field);
 	
 	public Map<String,String> getAll(String key);
 	
 	public Map<byte[],byte[]> getAll(byte[] key);
 	
 	public void hdel(String key,String field);
 	
 	public void hdel(byte[] key,byte[] field);
 	
 	public boolean hexists(String key,String field);
 	
 	public boolean hexists(byte[] key,byte[] field);
 	
 	public Set<String> hkeys(String key);
 	
 	public Set<byte[]> hkeys(byte[] key);
 	
 	
	//set表相关操作
 	public long sadd(byte[] key, byte[]... members);
 	
 	public long sadd(String key, String... members);
 	
 	public long srem(byte[] key, byte[]... members);
 	
 	public long srem(String key, String... members);
 	
 	public Set<String> smembers(String key);
 	
 	public Set<byte[]> smembers(byte[] key);
 	
 	
 	
 	

}
