package com.xiaoshu.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 
 * 功能说明：
 * 
 * SysLog.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月29日上午9:56:21
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
@TableName("sys_log")
public class SysLog {
	@TableId
	private Integer id;

	@TableField("user_id")
	private Integer userId;

	@TableField("content")
	private String content;

	@TableField("ip_address")
	private String ipAddress;

	@TableField("operation")
	private String operation;
	
	@TableField("operation_type")
	private String operationType;
	
	@TableField("cost_times")
	private Double costTimes;

	@TableField("create_time")
	private Date createTime;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public Double getCostTimes() {
		return costTimes;
	}

	public void setCostTimes(Double costTimes) {
		this.costTimes = costTimes;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
