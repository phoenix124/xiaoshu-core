package com.xiaoshu.model;

/**
 * 
 * 功能说明：
 * 
 * TokenModel.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:31:05
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
public class TokenModel {
	
	private String token;
	
	private Integer userId;

	public TokenModel() {
	}
	
	public TokenModel(String token,Integer userId ) {
		this.token = token;
		this.userId = userId;
	}
	
	
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return token+userId;
	}
	
	
	
}
