package com.xiaoshu.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xiaoshu.mapper.SystLogMapper;
import com.xiaoshu.model.SysLog;
import com.xiaoshu.service.SysLogService;

/**
 * 
 * 功能说明：
 * 
 * SysLogServiceImpl.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月29日上午10:04:27
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
@Service
public class SysLogServiceImpl extends  ServiceImpl<SystLogMapper, SysLog> implements SysLogService {

	@Override
	public void saveLog(Integer userId, String content, String ipAddress,
			String operation,String operationType,double costTimes) {
		SysLog log = new SysLog();
		log.setContent(content);
		log.setUserId(userId);
		log.setOperation(operation);
		log.setIpAddress(ipAddress);
		log.setOperationType(operationType);
		log.setCostTimes(costTimes);
		this.insert(log);
		System.out.println("保存数据成功----------------"+userId+"   "+content+"  "+ipAddress+"  "+operation);
		
	}

}
