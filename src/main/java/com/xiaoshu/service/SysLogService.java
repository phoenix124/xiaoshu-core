package com.xiaoshu.service;


import com.baomidou.mybatisplus.service.IService;
import com.xiaoshu.model.SysLog;

/**
 * 
 * 功能说明：
 * 
 * SysLogService.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月29日上午10:03:15
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
public interface SysLogService  extends IService<SysLog>{

	public void saveLog(Integer userId,String content,String ipAddress,String operation,String operationType,double costTimes);
}
