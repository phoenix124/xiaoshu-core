package com.xiaoshu.annotation;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 
 * 功能说明：记录日志的Annotation
 * 
 * Log.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年8月3日下午4:34:57
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
@Target({METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Log {
	/**
	 * 方法作用说明
	 * @return
	 */
	String value() default "";
	/**
	 * 方法类型说明
	 * @return
	 */
	LogType type() default LogType.NULL;
}
