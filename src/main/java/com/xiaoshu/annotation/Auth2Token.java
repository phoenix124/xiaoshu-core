package com.xiaoshu.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 功能说明：遵守Auth2.0协议，生成access_token,refresh_token,expire_time等
 * 根据Auth2.0协议进行拦截操作，对Access_token进行验证操作。
 * Auth2Token.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年9月4日上午10:35:11
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
@Target({TYPE,  METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Auth2Token {

	String value() default "";
}
