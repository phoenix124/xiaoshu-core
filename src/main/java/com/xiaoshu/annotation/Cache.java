package com.xiaoshu.annotation;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 功能说明：缓存方法结果集，如果缓存中命中，则不会执行该方法 。   *为防止数据不准确，请尽量设置过期时间为一个合理的值
 * 
 * Cache.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月7日上午10:46:47
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
@Target(METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Cache {

	//key值,同一个类中有相同方法名的时候必填，其他情况可不填，默认使用类名+方法名
	public String key() default "";
	
	//过期时间 单位：秒，默认为30天
	public int expire() default -1;
}
