/**
*	Copyright © 2014 - 2017 小树盛凯科技  小树盛凯 Tech. All Rights Reserved 
*/
package com.xiaoshu.annotation;

/**
 * 功能说明：
 * 
 * CacheType.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:06:11
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
public enum CacheType {

    /**
     * 查询时使用缓存
     */
    QUERY,
    /**
     * 在进行 增、删、改的时候通知缓存
     */
    NOTIFY
}
