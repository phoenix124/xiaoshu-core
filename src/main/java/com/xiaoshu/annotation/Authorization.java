/**
*	Copyright © 2014 - 2017 小树盛凯科技  小树盛凯 Tech. All Rights Reserved 
*/
package com.xiaoshu.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能说明：
 * 
 * Authorization.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:03:39
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Authorization {
    
    String value() default "";
}
