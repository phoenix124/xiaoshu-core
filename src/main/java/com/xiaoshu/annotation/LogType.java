
package com.xiaoshu.annotation;

/**
 * 
 * 功能说明：记录日志的类型，包含增、删、改、查和接口调用等操作
 * 
 * LogType.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年8月3日下午4:24:09
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
public enum LogType {
	/**
	 * 查询操作
	 */
	QUERY,
	/**
	 * 更新操作
	 */
	UPDATE,
	/**
	 * 删除操作
	 */
	DELETE,
	/**
	 * 插入操作
	 */
	INSERT,
	/**
	 * 接口调用操作
	 */
	INTERFACE,
	/**
	 * Dubbo接口调用
	 */
	DUBBO,
	/**
	 * 不确定操作类型
	 */
	NULL

}
