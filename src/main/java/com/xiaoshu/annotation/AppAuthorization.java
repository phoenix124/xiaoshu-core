package com.xiaoshu.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 功能说明：APP 上报信息进行鉴权
 * 
 * @AppAuthorization.java
 * 
 * Original Author: deane.jia,@2017年10月12日@上午7:57:08
 * 
 * Copyright (C)2014-@2017 小树盛凯科技 All rights reserved.
 */
@Target({TYPE,  METHOD, })
@Retention(RetentionPolicy.RUNTIME)
public @interface AppAuthorization {

	String value() default "";
}
