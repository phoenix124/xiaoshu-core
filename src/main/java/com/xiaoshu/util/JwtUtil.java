package com.xiaoshu.util;



import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;

public class JwtUtil {

	
	private static final String APIKEY = "ubtechinc@robot.com";
	
	
	public static String createJwt(String id,String subject,long ttlMillis) {
		
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

	    //We will sign our JWT with our ApiKey secret
	    byte[] apiKeySecretBytes = APIKEY.getBytes();
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

	    //Let's set the JWT Claims
	    JwtBuilder builder = Jwts.builder().setId(id)
	                                .setIssuedAt(now)
	                                .setSubject(subject)
	                                .signWith(signatureAlgorithm, signingKey);

	    //if it has been specified, let's add the expiration
	    if (ttlMillis >= 0) {
	    long expMillis = nowMillis + ttlMillis;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	    }

	    //Builds the JWT and serializes it to a compact, URL-safe string
	    return builder.compact();
	}
	
	
	
	
	public static Claims parseJwt(String jwt){
		 //This line will throw an exception if it is not a signed JWS (as expected)
		Claims claims=null;
		try {
			claims = Jwts.parser().setSigningKey(APIKEY.getBytes())
							.parseClaimsJws(jwt).getBody();
		} catch (Exception e) {
			System.out.println("token 校验出错");
		}
			
		
		return claims;
	}
	
	
	public static void main(String[] args) {
		String token = createJwt("1000","test",-1);
		System.out.println(token);
		
		Claims claims = parseJwt(token);
		if(claims!=null){
			String id = claims.getId();
			Date date = claims.getExpiration();
			System.out.println(id+" "+date);
		}
	}
}
