/**
*   Copyright © 2014 - 2017 小树盛凯科技  小树盛凯 Tech. All Rights Reserved 
*/
package com.xiaoshu.util;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.io.UnsafeInput;
import com.esotericsoftware.kryo.io.UnsafeOutput;

/**
 * 
 * 功能说明：
 * 
 * KryoUtil.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:33:35
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
public class KryoUtil{
	private static final int OUTPUT_BUFFER_SIZE = 1024 * 8; // 8k 
	private final static Log logger = LogFactory.getLog( KryoUtil.class );
	
	//使用kryo工具序列化对�?
	public static byte[] serialize(Object obj) {
		Kryo kryo = new Kryo();
		byte[] data = null;
		Output kryoOut = null;
		try {
			byte[] out = new byte[OUTPUT_BUFFER_SIZE];
			kryoOut = new UnsafeOutput(out);
			kryo.writeObject(kryoOut, obj);
			kryoOut.flush();
			data = kryoOut.toBytes();
		} catch (Exception e) {
			logger.error("序列化对象失败：" + obj);
			e.printStackTrace();
		}finally{
			if(kryoOut!=null)kryoOut.close();
		}
		return data;
	}
	
	public static <T> T deserialize( byte[] data, Class<T> myClass ) throws Exception{
		Kryo kryo = new Kryo();
		Input kryoIut = null;
		T obj = null;
		try {
			kryoIut = new UnsafeInput(data);
			obj = kryo.readObjectOrNull(kryoIut, myClass);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(kryoIut!=null)kryoIut.close();
		}
		return obj;
	}
}
