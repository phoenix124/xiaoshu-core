package com.xiaoshu.util;



import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 
 * 功能说明：
 * 
 * HttpHelper.java
 * 
 * Original Author: shengkai.jia-贾盛凯,2017年6月7日上午10:21:35
 * 
 * Copyright (C)2012-2017 深圳小树盛凯科技 All rights reserved.
 */
public class HttpHelper {

	public static HttpServletRequest getHttpServletRequest() {
		return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
	}
}
