package com.xiaoshu.constant;

/**
 * 
 * 功能说明：
 * 
 * Constants.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:27:07
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
public class Constants {

	 /**
    * 存储当前登录用户id的字段名
    */
   public static final String CURRENT_USER_ID = "CURRENT_USER_ID";

   
   /**
    * token有效期（72小时）
    */
   public static final int TOKEN_EXPIRES_HOUR = 72*3600;


   /**
    * 存放Authorization的header字段
    */
   public static final String AUTHORIZATION = "authorization";
   
   /**
    * 存放APP的Token的前缀字段
    */
   public static final String APP_TOKEN_KEY_PREFIX="app:token:";
}
