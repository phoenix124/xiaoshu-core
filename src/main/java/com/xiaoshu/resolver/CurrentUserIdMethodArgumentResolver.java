/**
*   Copyright © 2014 - 2017 小树盛凯科技  小树盛凯 Tech. All Rights Reserved 
*/
package com.xiaoshu.resolver;

import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import com.xiaoshu.annotation.CurrentUserId;
import com.xiaoshu.constant.Constants;

/**
 * 
 * 功能说明：
 * 
 * CurrentUserIdMethodArgumentResolver.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:34:14
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Component
public class CurrentUserIdMethodArgumentResolver implements HandlerMethodArgumentResolver {

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		if(parameter.hasParameterAnnotation(CurrentUserId.class)){
			return true;
		}
		return false;
	}

	@Override
	public Object resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {
		 //取出鉴权时存入的登录用户Id
		Integer userId = (Integer) webRequest.getAttribute(Constants.CURRENT_USER_ID, RequestAttributes.SCOPE_REQUEST);
		
		if(userId!=null){
			return userId;
		}
		
		throw new MissingServletRequestPartException(Constants.CURRENT_USER_ID);
	}

}
