/**
*	Copyright © 2014 - 2017 小树盛凯科技  小树盛凯 Tech. All Rights Reserved 
*/
package com.xiaoshu.config.dubbo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ProtocolConfig;
import com.alibaba.dubbo.config.ProviderConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.rpc.Exporter;
/**
 * 功能说明：
 * 
 * DubboConfig.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:17:27
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Configuration
@EnableAutoConfiguration
@ConditionalOnBean(DubboProperties.class)
@AutoConfigureAfter(DubboProperties.class)
@ConditionalOnClass(Exporter.class)
public class DubboConfig {

    
    @Autowired
    private  DubboProperties properties;
    
    @Autowired
    private Environment environment;
     /**
     * dubbo服务提供
     * 
     * @return
     */
    @Bean
    public ProviderConfig providerConfig(){
        ProviderConfig config = new ProviderConfig();
        
        config.setTimeout(properties.getProviderTimeout());
        
        config.setRetries(properties.getProviderRetries());
        
        config.setDelay(properties.getProviderDelay());
        
        config.setFilter("-exception");
        
        //config.setApplication(applicationConfig());
        
        //config.setRegistry(registryConfig());
        
        //config.setProtocols(protocolConfig());
        
        return config;
    }

    /*private List<ProtocolConfig> protocolConfigs() {
         // 服务提供者协议配置
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setName(properties.getProtocolName());
        protocolConfig.setPort(properties.getProtocolPort());
        protocolConfig.setThreads(200);
        System.out.println("默认protocolConfig：" + protocolConfig.hashCode());
        List<ProtocolConfig> list = new ArrayList<ProtocolConfig>();
        list.add(protocolConfig);
        return list;
    }*/
    
    // 协议配置
    @Bean
    public ProtocolConfig protocolConfig() {
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setName(properties.getProtocolName());
        protocolConfig.setPort(properties.getProtocolPort());
        protocolConfig.setThreads(200);
        System.out.println("默认protocolConfig：" + protocolConfig.hashCode());
        return protocolConfig;
    }
    
    @Bean
    @ConditionalOnProperty(name="dubbo.protocol-hession", havingValue="hession")
    public ProtocolConfig hessionProtocolConfig() {
    	ProtocolConfig protocolConfig = new ProtocolConfig();
    	protocolConfig.setName("hessian");
    	protocolConfig.setPort(Integer.valueOf(environment.getProperty("dubbo.protocol-hession-port")));
    	protocolConfig.setThreads(200);
    	protocolConfig.setContextpath("dubbo");
    	protocolConfig.setServer("servlet");
    	System.out.println("默认protocolConfig：" + protocolConfig.hashCode());
    	return protocolConfig;
    }

    // 连接注册中心配置
    @Bean
    public  RegistryConfig registryConfig() {
        RegistryConfig registry = new RegistryConfig();
        registry.setProtocol(properties.getRegistryProtocol());
        registry.setAddress(properties.getRegistryAddress());
        registry.setRegister(true);
        registry.setSubscribe(true);
        return registry;
    }

    // 当前应用配置
    @Bean
    public ApplicationConfig applicationConfig() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName(properties.getApplicationName());
        return applicationConfig;
    }

    
    /*@Bean
    public ConsumerConfig consumerConfig(){
        ConsumerConfig config = new ConsumerConfig();
        config.setApplication(applicationConfig());
        return config;
    }*/
    
    
    /**
     * 设置dubbo扫描包
     * @return AnnotationBean
     */
    /*@Bean
    public static  AnnotationBean annotationBean(@Value("${dubbo.scan-package}") String packageName){
        AnnotationBean annotation = new AnnotationBean();
        
        
        //annotation.setPackage(properties.getScanPackage());
        annotation.setPackage("com.ubtechinc.service");
        annotation.setPackage(packageName);
        return annotation;
    }*/
}
