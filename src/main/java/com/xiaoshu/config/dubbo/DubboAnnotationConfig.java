/**
*	Copyright © 2014 - 2017 小树盛凯科技  小树盛凯 Tech. All Rights Reserved 
*/
package com.xiaoshu.config.dubbo;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.dubbo.config.spring.AnnotationBean;

/**
 * 功能说明：
 * 
 * DubboAnnotationConfig.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:16:46
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Configuration
@EnableAutoConfiguration
@ConditionalOnBean(DubboProperties.class)
@AutoConfigureAfter(DubboProperties.class)
public class DubboAnnotationConfig extends AnnotationBean implements Serializable{

    
    private static final long serialVersionUID = 1L;

    public DubboAnnotationConfig() {
        System.out.println("init DubboAnnotationConfig**********************************");
    }
    
    @Autowired
    private  DubboProperties properties;
    
    
    /**
     * 设置dubbo扫描包
     * @return AnnotationBean
     */
    @Override
    public void setPackage(String annotationPackage) {
        
        super.setPackage(properties.getScanPackage());
    }
}
