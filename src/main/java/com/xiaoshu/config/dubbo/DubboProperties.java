/**
 * Copyright © 2014 - 2017 小树盛凯科技 小树盛凯 Tech. All Rights Reserved
 */
package com.xiaoshu.config.dubbo;

import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 功能说明：
 * 
 * DubboProperties.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:18:14
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Component
@ConditionalOnResource(resources = { "classpath:dubbo.properties" })
@ConfigurationProperties(prefix = "dubbo")
@PropertySource(value = "classpath:dubbo.properties")
public class DubboProperties {

	private String applicationName;

	private String logger;

	private String registryProtocol;

	private String registryAddress;

	private String protocolName;

	private int protocolPort;

	private int providerTimeout;

	private int providerRetries;

	private int providerDelay;

	private String scanPackage;

	public String getScanPackage() {
		return scanPackage;
	}

	public void setScanPackage(String scanPackage) {
		this.scanPackage = scanPackage;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getLogger() {
		return logger;
	}

	public void setLogger(String logger) {
		this.logger = logger;
	}

	public String getRegistryProtocol() {
		return registryProtocol;
	}

	public void setRegistryProtocol(String registryProtocol) {
		this.registryProtocol = registryProtocol;
	}

	public String getRegistryAddress() {
		return registryAddress;
	}

	public void setRegistryAddress(String registryAddress) {
		this.registryAddress = registryAddress;
	}

	public String getProtocolName() {
		return protocolName;
	}

	public void setProtocolName(String protocolName) {
		this.protocolName = protocolName;
	}

	public int getProtocolPort() {
		return protocolPort;
	}

	public void setProtocolPort(int protocolPort) {
		this.protocolPort = protocolPort;
	}

	public int getProviderTimeout() {
		return providerTimeout;
	}

	public void setProviderTimeout(int providerTimeout) {
		this.providerTimeout = providerTimeout;
	}

	public int getProviderRetries() {
		return providerRetries;
	}

	public void setProviderRetries(int providerRetries) {
		this.providerRetries = providerRetries;
	}

	public int getProviderDelay() {
		return providerDelay;
	}

	public void setProviderDelay(int providerDelay) {
		this.providerDelay = providerDelay;
	}
}
