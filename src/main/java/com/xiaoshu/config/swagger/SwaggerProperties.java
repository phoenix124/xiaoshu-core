/**
 * Copyright © 2014 - 2017 小树盛凯科技 小树盛凯 Tech. All Rights Reserved
 */
package com.xiaoshu.config.swagger;

import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 
 * 功能说明：
 * 
 * SwaggerProperties.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:26:01
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Component
@ConditionalOnResource(resources={"classpath:swagger.properties"})
@ConfigurationProperties(prefix="swagger")
@PropertySource("classpath:swagger.properties")
public class SwaggerProperties {

	private String basePackage;
	
	private String contactName;
	
	private String contactUrl;
	
	private String contactEmail;

	
	
	public String getContactUrl() {
		return contactUrl;
	}

	public void setContactUrl(String contactUrl) {
		this.contactUrl = contactUrl;
	}

	public String getBasePackage() {
		return basePackage;
	}

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	
	
}
