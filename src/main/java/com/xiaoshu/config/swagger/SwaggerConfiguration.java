/**
 * Copyright © 2014 - 2017 小树盛凯科技 小树盛凯 Tech. All Rights Reserved
 */
package com.xiaoshu.config.swagger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.annotations.Api;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * 功能说明：
 * 
 * SwaggerConfiguration.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:25:46
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Configuration
@EnableSwagger2
@EnableConfigurationProperties
@ConditionalOnBean(SwaggerProperties.class)
@AutoConfigureAfter(SwaggerProperties.class)
public class SwaggerConfiguration {

    @Autowired
    private SwaggerProperties properties;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("group1").apiInfo(apiInfo())
                .select()
                // .apis(RequestHandlerSelectors.basePackage(properties.getBasePackage()))
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.any()).build();

    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring Boot中使用Swagger2构建RESTful APIs")
                .description("更多Spring Boot相关文章请关注：http://blog.didispace.com/")
                .termsOfServiceUrl("http://blog.didispace.com/")
                .contact(
                        new Contact(properties.getContactName(), properties.getContactUrl(),
                                properties.getContactEmail())).version("1.0.0").build();
    }
}
