/**
 * Copyright © 2014 - 2017 小树盛凯科技 小树盛凯 Tech. All Rights Reserved
 */
package com.xiaoshu.config.druid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;

/**
 * 功能说明：
 * 
 * DruidConfigruation.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:13:17
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */

@Configuration
@EnableConfigurationProperties
@ConditionalOnBean(DruidProperties.class)
@AutoConfigureAfter(DruidProperties.class)
public class DruidConfigruation {

	@Autowired
	private DruidProperties properties;

	@Bean
	public DruidDataSource getDruidDataSource() {
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setUrl(properties.getUrl());
		dataSource.setPassword(properties.getPassword());
		dataSource.setUsername(properties.getUsername());
		dataSource.setDriverClassName(properties.getDriverClassName());
		return dataSource;
	}

	@Bean
	public ServletRegistrationBean druidServlet() {
		return new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
	}

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(new WebStatFilter());
		filterRegistrationBean.addUrlPatterns("/*");
		filterRegistrationBean.addInitParameter("exclusions",
				"*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*");
		return filterRegistrationBean;
	}
}
