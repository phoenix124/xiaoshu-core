/**
 * Copyright © 2014 - 2017 小树盛凯科技 小树盛凯 Tech. All Rights Reserved
 */
package com.xiaoshu.config.druid;

import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 功能说明：
 * 
 * DruidProperties.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:12:29
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Component
@ConditionalOnResource(resources={"classpath:druid.properties"})
@ConfigurationProperties(prefix = "druid")
@PropertySource("classpath:druid.properties")
public class DruidProperties {

    private String url;

    private String password;

    private String username;

    private String driverClassName;



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }
}
