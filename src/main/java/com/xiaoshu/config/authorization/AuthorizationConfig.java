/**
*	Copyright © 2014 - 2017 小树盛凯科技  小树盛凯 Tech. All Rights Reserved 
*/
package com.xiaoshu.config.authorization;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.xiaoshu.interceptor.AuthorizationInterceptor;
import com.xiaoshu.resolver.CurrentUserIdMethodArgumentResolver;

/**
 * 功能说明：
 * 
 * AuthorizationConfig.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:10:31
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Configuration
public class AuthorizationConfig extends WebMvcConfigurerAdapter{

    
    @Autowired
    private AuthorizationInterceptor interceptor;

    @Autowired
    private CurrentUserIdMethodArgumentResolver currentUserIdResolver;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor);

    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(currentUserIdResolver);
    }
}
