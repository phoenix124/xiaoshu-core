/**
*	Copyright © 2014 - 2017 小树盛凯科技  小树盛凯 Tech. All Rights Reserved 
*/
package com.xiaoshu.config.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xiaoshu.manager.BilinShardedJedisPool;

import redis.clients.jedis.JedisPoolConfig;
/**
 * 功能说明：
 * 
 * RedisConfiguration.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:21:57
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Configuration
@EnableConfigurationProperties
@ConditionalOnBean(JedisProperties.class)
@AutoConfigureAfter(JedisProperties.class)
public class RedisConfiguration {
    @Autowired
    private JedisProperties jedisProperties;

    @Bean(name = "jedis.pool")
    public BilinShardedJedisPool jedisPool(@Qualifier("jedis.pool.config") JedisPoolConfig config) {
        return new BilinShardedJedisPool(config, jedisProperties.getConfigstr(), jedisProperties.getTimeout(),
                jedisProperties.getPassword());
    }
    /*
     * @Bean(name = "jedis.cluster") public BindJedisCluster jedisCluster(
     * 
     * @Qualifier("jedis.pool.config") JedisPoolConfig config,
     * 
     * @Value("${jedis.pool.configstr}") String configstr,
     * 
     * @Value("${jedis.pool.password}") String password,
     * 
     * @Value("${jedis.pool.timeout}") int timeout) { return new
     * BindJedisCluster(config,configstr,timeout,password); }
     */

    @Bean(name = "jedis.pool.config")
    public JedisPoolConfig jedisPoolConfig() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(jedisProperties.getMaxTotal());
        config.setMaxIdle(jedisProperties.getMaxIdle());
        config.setMaxWaitMillis(jedisProperties.getMaxWaitMillis());
        config.setLifo(true);
        return config;
    }
}
