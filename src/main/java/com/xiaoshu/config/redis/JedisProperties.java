/**
 * Copyright © 2014 - 2017 小树盛凯科技 小树盛凯 Tech. All Rights Reserved
 */
package com.xiaoshu.config.redis;

import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
/**
 * 功能说明：
 * 
 * JedisProperties.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:21:06
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Component
@ConditionalOnResource(resources={"classpath:jedis.properties"})
@ConfigurationProperties(prefix = "jedis.pool")
@PropertySource("classpath:jedis.properties")
public class JedisProperties {
    private String configstr;

    private String password;

    private int timeout;

    private int maxTotal;

    private int maxIdle;

    private int maxWaitMillis;

    public String getConfigstr() {
        return configstr;
    }

    public void setConfigstr(String configstr) {
        this.configstr = configstr;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
    }

    public int getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
    }

    public int getMaxWaitMillis() {
        return maxWaitMillis;
    }

    public void setMaxWaitMillis(int maxWaitMillis) {
        this.maxWaitMillis = maxWaitMillis;
    }
}
