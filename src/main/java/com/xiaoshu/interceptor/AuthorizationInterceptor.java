package com.xiaoshu.interceptor;


import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSONObject;
import com.xiaoshu.annotation.Authorization;
import com.xiaoshu.constant.Constants;
import com.xiaoshu.manager.TokenManager;
import com.xiaoshu.model.TokenModel;

/**
 * 
 * 功能说明：//拦截被@Authorization注解的类和方法，进行相应的处理操作后返回
 * 
 * AuthorizationInterceptor.java
 * 
 * Original Author: deane.jia,2017年5月2日 下午5:43:23
 * 
 * Copyright (C)2014-2017 小树盛凯科技 All rights reserved.
 */
@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter{

	@Autowired
	private TokenManager tokenManger;
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		//System.out.println("AuthorizationInterceptor-----------------------------");
		 //如果不是映射到方法直接通过
		if(!(handler instanceof HandlerMethod)){
			return true;
		}
		HandlerMethod method = (HandlerMethod) handler;
		//如果方法和类中都没有@Authorization注解，则直接跳过
		if(method.getBean().getClass().getAnnotation(Authorization.class)==null
				&&method.getMethodAnnotation(Authorization.class)==null){
			return true;
		}
		//从head中获取验证信息
		String authentication = request.getHeader(Constants.AUTHORIZATION);
		
		TokenModel tokenModel = tokenManger.getToken(authentication);
		if(!tokenManger.checkToken(tokenModel)){
			 response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			 ServletOutputStream os = response.getOutputStream();
			 JSONObject json = new JSONObject();
			 json.put("code", "-1");
			 json.put("msg", "token验证错误,请重新登录");
			 json.put("data", null);
			 os.write(json.toJSONString().getBytes("utf-8"));
			 os.close();
			return false;
		}
		//将userId放入request中
		request.setAttribute(Constants.CURRENT_USER_ID, tokenModel.getUserId());
		return true;
	}
}
